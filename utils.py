# ==========================================
# Author: Rohan Chikkam
# Date:   28 Jul 2022
# ==========================================

import yaml


class validate_config:
    def __init__(self, path):
        self.path = path
        with open(path, "r") as config_file:
            self.yaml_content = yaml.safe_load(config_file)  # Attempt to load YAML file

    def test_Name(self):  # Check if name exists in YML config
        return "name" in self.yaml_content and self.yaml_content["name"] != None

    def test_ConnDetails(self):  # Check if connection details exist in YML config
        return (((
            "host" in self.yaml_content
            and self.yaml_content["host"] != None
            and "user" in self.yaml_content
            and self.yaml_content["user"] != None
            and "pass" in self.yaml_content
            and self.yaml_content["pass"] != None
            and "db" in self.yaml_content
            and self.yaml_content["db"] != None
        ) or (
            "kv" in self.yaml_content
            and self.yaml_content["kv"] != None
            and "token" in self.yaml_content
            and self.yaml_content["token"] != None
            and "path" in self.yaml_content
            and self.yaml_content["path"] != None
        )) and "port" in self.yaml_content
            and self.yaml_content["port"] != None)
