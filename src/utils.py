# ==========================================
# Author: Rohan Chikkam
# Date:   22 Jul 2022
# ==========================================

import pandas as pd
import re

class sql_functions:
    def __init__(self, rollback_on_error, continue_on_error, conn):
        self.rollback_on_error = rollback_on_error
        self.continue_on_error = continue_on_error
        self.conn = conn

    def execute_sql(self, query):
        cursor = self.conn.cursor()
        print("\nRUNNING: {}".format(query))
        try:
            if not query.startswith("-"):    
                cursor.execute(query)
                try:
                    data = cursor.fetchall()
                    return str(data)
                except:
                    pass
                self.conn.commit()
            else:
                print("Ignored")
        except Exception as e:
            print("ENCOUNTERED ERROR: {}".format(e))
            if(self.rollback_on_error):
                print("ROLLING BACK")
                self.conn.rollback()
            if(not self.continue_on_error):
                print("HALTING HERE")
                pass  # To implement
            return str(e)

    def run_sql_file(self, file_name, test_name):
        file_path = "{}/{}.sql".format(test_name, file_name)
        input_data = open(file_path, "r")
        query = input_data.read()

        query = re.sub(r'^-.*\n?', '', query, flags=re.MULTILINE)

        rtn = ""

        for statement in query.split(";")[0:-1]:
            data = self.execute_sql(statement)
            if data != None:
                rtn += data
        
        return rtn

    def fill_table_data(self, file_name, test_name):
        file_path = "{}/{}.csv".format(test_name, file_name)
        input_data = pd.read_csv(file_path)
        input_df = pd.DataFrame(input_data)

        for i, row in input_df.iterrows():
            sql = "INSERT INTO {} VALUES {}".format(
                file_name, tuple(row))
            self.execute_sql(sql)

    def is_table_entries_equal(self, table1, table2):
        query = """
                SELECT * 
                FROM {}
            """

        print("\nRUNNING: {}".format(query.format(
            table1
        )))
        sql_query_table1 = pd.read_sql_query(
            query.format(
                table1
            ),
            self.conn,
        )

        print("\nRUNNING: {}".format(query.format(
            table2
        )))
        sql_query_table2 = pd.read_sql_query(
            query.format(
                table2
            ),
            self.conn,
        )

        output_df_table1 = pd.DataFrame(sql_query_table1)
        output_df_table2 = pd.DataFrame(sql_query_table2)

        return output_df_table1.equals(output_df_table2)

    def is_table_columns_equal(self, table1, table2):
        query = """
        SELECT OWNER,
        COLUMN_NAME,
        DATA_TYPE,
        DATA_LENGTH,
        DATA_PRECISION,
        DATA_SCALE,
        NULLABLE,
        COLUMN_ID,
        DEFAULT_LENGTH,
        DATA_DEFAULT,
        DEFAULT_ON_NULL,
        IDENTITY_COLUMN,
        COLLATION FROM all_tab_columns WHERE table_name = UPPER('{}') ORDER BY column_id
        """

        ddl = self.execute_sql(query.format(table1))
        expected_ddl = self.execute_sql(query.format(table2))

        print(ddl)
        print(expected_ddl)

        return expected_ddl == ddl

    def is_table_indexes_equal(self, table1, table2):
        query = """
            SELECT OWNER,
            INDEX_TYPE,
            TABLE_TYPE,
            UNIQUENESS,
            DROPPED,
            INDEXING FROM all_indexes   WHERE table_name = UPPER('{}')
            """

        ddl = self.execute_sql(query.format(table1))
        expected_ddl = self.execute_sql(query.format(table2))

        print(ddl)
        print(expected_ddl)

        return expected_ddl == ddl

    def is_table_constraints_equal(self, table1, table2):
        query = """
            SELECT OWNER,
            CONSTRAINT_TYPE,
            SEARCH_CONDITION
            VIEW_RELATED FROM all_constraints WHERE table_name = UPPER('{}')
            """

        ddl = self.execute_sql(query.format(table1))
        expected_ddl = self.execute_sql(query.format(table2))

        print(ddl)
        print(expected_ddl)

        return expected_ddl == ddl
